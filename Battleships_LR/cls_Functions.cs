﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Battleships_LR
{
    public static class cls_Functions
    {
        public static bool NextBool(this Random r, int truePercentage = 50)
        {
            return r.NextDouble() < truePercentage / 100.0;
        }

        public static string Right(this string value, int length)
        {
            return value.Substring(value.Length - length);
        }

        public static string Left(this string value, int length)
        {
            return value.Substring(0,length);
        }

        public static int GetColumnNumber(string columnName)
        {
            char[] characters = columnName.ToUpperInvariant().ToCharArray();
            int sum = 0;
            for (int i = 0; i < characters.Length; i++)
            {
                sum *= 26;
                sum += (characters[i] - 'A' + 1);
            }
            return sum;  // in this example, sum would be "1" representing the column # where Customer Name resides 
        }

        public static void AttachKeyDownToControls(Control ccoll)
        {
            foreach (Control c in ccoll.Controls)
            {
                if (0 < c.Controls.Count)
                {
                    AttachKeyDownToControls(c);
                }
                else
                {
                    c.KeyDown += c_KeyDown;
                }
            }
        }

        public static void c_KeyDown(object sender, KeyEventArgs e)
        {
            Control c = sender as Control;
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    c.Parent.SelectNextControl(c, true, true, true, true);
                    e.Handled = true;
                    break;
            }
        }
    }
}
