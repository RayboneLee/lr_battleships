﻿namespace Battleships_LR
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txt_Guess = new System.Windows.Forms.TextBox();
            this.lbl_Guess = new System.Windows.Forms.Label();
            this.cmb_Fire = new System.Windows.Forms.Button();
            this.pbxJ10 = new System.Windows.Forms.PictureBox();
            this.pbxI10 = new System.Windows.Forms.PictureBox();
            this.pbxH10 = new System.Windows.Forms.PictureBox();
            this.pbxG10 = new System.Windows.Forms.PictureBox();
            this.pbxF10 = new System.Windows.Forms.PictureBox();
            this.pbxE10 = new System.Windows.Forms.PictureBox();
            this.pbxD10 = new System.Windows.Forms.PictureBox();
            this.pbxC10 = new System.Windows.Forms.PictureBox();
            this.pbxB10 = new System.Windows.Forms.PictureBox();
            this.pbxA10 = new System.Windows.Forms.PictureBox();
            this.pbxJ9 = new System.Windows.Forms.PictureBox();
            this.pbxI9 = new System.Windows.Forms.PictureBox();
            this.pbxH9 = new System.Windows.Forms.PictureBox();
            this.pbxG9 = new System.Windows.Forms.PictureBox();
            this.pbxF9 = new System.Windows.Forms.PictureBox();
            this.pbxE9 = new System.Windows.Forms.PictureBox();
            this.pbxD9 = new System.Windows.Forms.PictureBox();
            this.pbxC9 = new System.Windows.Forms.PictureBox();
            this.pbxB9 = new System.Windows.Forms.PictureBox();
            this.pbxA9 = new System.Windows.Forms.PictureBox();
            this.pbxJ8 = new System.Windows.Forms.PictureBox();
            this.pbxI8 = new System.Windows.Forms.PictureBox();
            this.pbxH8 = new System.Windows.Forms.PictureBox();
            this.pbxG8 = new System.Windows.Forms.PictureBox();
            this.pbxF8 = new System.Windows.Forms.PictureBox();
            this.pbxE8 = new System.Windows.Forms.PictureBox();
            this.pbxD8 = new System.Windows.Forms.PictureBox();
            this.pbxC8 = new System.Windows.Forms.PictureBox();
            this.pbxB8 = new System.Windows.Forms.PictureBox();
            this.pbxA8 = new System.Windows.Forms.PictureBox();
            this.pbxJ7 = new System.Windows.Forms.PictureBox();
            this.pbxI7 = new System.Windows.Forms.PictureBox();
            this.pbxH7 = new System.Windows.Forms.PictureBox();
            this.pbxG7 = new System.Windows.Forms.PictureBox();
            this.pbxF7 = new System.Windows.Forms.PictureBox();
            this.pbxE7 = new System.Windows.Forms.PictureBox();
            this.pbxD7 = new System.Windows.Forms.PictureBox();
            this.pbxC7 = new System.Windows.Forms.PictureBox();
            this.pbxB7 = new System.Windows.Forms.PictureBox();
            this.pbxA7 = new System.Windows.Forms.PictureBox();
            this.pbxJ6 = new System.Windows.Forms.PictureBox();
            this.pbxI6 = new System.Windows.Forms.PictureBox();
            this.pbxH6 = new System.Windows.Forms.PictureBox();
            this.pbxG6 = new System.Windows.Forms.PictureBox();
            this.pbxF6 = new System.Windows.Forms.PictureBox();
            this.pbxE6 = new System.Windows.Forms.PictureBox();
            this.pbxD6 = new System.Windows.Forms.PictureBox();
            this.pbxC6 = new System.Windows.Forms.PictureBox();
            this.pbxB6 = new System.Windows.Forms.PictureBox();
            this.pbxA6 = new System.Windows.Forms.PictureBox();
            this.pbxJ5 = new System.Windows.Forms.PictureBox();
            this.pbxI5 = new System.Windows.Forms.PictureBox();
            this.pbxH5 = new System.Windows.Forms.PictureBox();
            this.pbxG5 = new System.Windows.Forms.PictureBox();
            this.pbxF5 = new System.Windows.Forms.PictureBox();
            this.pbxE5 = new System.Windows.Forms.PictureBox();
            this.pbxD5 = new System.Windows.Forms.PictureBox();
            this.pbxC5 = new System.Windows.Forms.PictureBox();
            this.pbxB5 = new System.Windows.Forms.PictureBox();
            this.pbxA5 = new System.Windows.Forms.PictureBox();
            this.pbxJ4 = new System.Windows.Forms.PictureBox();
            this.pbxI4 = new System.Windows.Forms.PictureBox();
            this.pbxH4 = new System.Windows.Forms.PictureBox();
            this.pbxG4 = new System.Windows.Forms.PictureBox();
            this.pbxF4 = new System.Windows.Forms.PictureBox();
            this.pbxE4 = new System.Windows.Forms.PictureBox();
            this.pbxD4 = new System.Windows.Forms.PictureBox();
            this.pbxC4 = new System.Windows.Forms.PictureBox();
            this.pbxB4 = new System.Windows.Forms.PictureBox();
            this.pbxA4 = new System.Windows.Forms.PictureBox();
            this.pbxJ3 = new System.Windows.Forms.PictureBox();
            this.pbxI3 = new System.Windows.Forms.PictureBox();
            this.pbxH3 = new System.Windows.Forms.PictureBox();
            this.pbxG3 = new System.Windows.Forms.PictureBox();
            this.pbxF3 = new System.Windows.Forms.PictureBox();
            this.pbxE3 = new System.Windows.Forms.PictureBox();
            this.pbxD3 = new System.Windows.Forms.PictureBox();
            this.pbxC3 = new System.Windows.Forms.PictureBox();
            this.pbxB3 = new System.Windows.Forms.PictureBox();
            this.pbxA3 = new System.Windows.Forms.PictureBox();
            this.pbxJ2 = new System.Windows.Forms.PictureBox();
            this.pbxI2 = new System.Windows.Forms.PictureBox();
            this.pbxH2 = new System.Windows.Forms.PictureBox();
            this.pbxG2 = new System.Windows.Forms.PictureBox();
            this.pbxF2 = new System.Windows.Forms.PictureBox();
            this.pbxE2 = new System.Windows.Forms.PictureBox();
            this.pbxD2 = new System.Windows.Forms.PictureBox();
            this.pbxC2 = new System.Windows.Forms.PictureBox();
            this.pbxB2 = new System.Windows.Forms.PictureBox();
            this.pbxA2 = new System.Windows.Forms.PictureBox();
            this.pbxJ1 = new System.Windows.Forms.PictureBox();
            this.pbxI1 = new System.Windows.Forms.PictureBox();
            this.pbxH1 = new System.Windows.Forms.PictureBox();
            this.pbxG1 = new System.Windows.Forms.PictureBox();
            this.pbxF1 = new System.Windows.Forms.PictureBox();
            this.pbxE1 = new System.Windows.Forms.PictureBox();
            this.pbxD1 = new System.Windows.Forms.PictureBox();
            this.pbxC1 = new System.Windows.Forms.PictureBox();
            this.pbxB1 = new System.Windows.Forms.PictureBox();
            this.pbxA1 = new System.Windows.Forms.PictureBox();
            this.pbx_Grid = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pbxJ10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxI10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxH10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxG10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxF10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxE10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxD10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxC10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxB10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxA10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxJ9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxI9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxH9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxG9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxF9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxE9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxD9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxC9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxB9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxA9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxJ8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxI8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxH8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxG8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxF8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxE8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxD8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxC8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxB8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxA8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxJ7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxI7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxH7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxG7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxF7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxE7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxD7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxC7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxB7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxA7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxJ6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxI6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxH6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxG6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxF6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxE6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxD6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxC6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxB6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxA6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxJ5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxI5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxH5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxG5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxF5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxE5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxD5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxC5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxB5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxA5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxJ4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxI4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxH4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxG4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxF4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxE4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxD4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxC4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxB4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxA4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxJ3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxI3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxH3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxG3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxF3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxE3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxD3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxC3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxB3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxA3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxJ2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxI2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxH2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxG2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxF2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxE2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxD2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxC2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxB2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxA2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxJ1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxI1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxH1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxG1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxF1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxE1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxD1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxC1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxB1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxA1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_Grid)).BeginInit();
            this.SuspendLayout();
            // 
            // txt_Guess
            // 
            this.txt_Guess.Location = new System.Drawing.Point(131, 77);
            this.txt_Guess.Name = "txt_Guess";
            this.txt_Guess.Size = new System.Drawing.Size(45, 20);
            this.txt_Guess.TabIndex = 0;
            // 
            // lbl_Guess
            // 
            this.lbl_Guess.AutoSize = true;
            this.lbl_Guess.Location = new System.Drawing.Point(21, 80);
            this.lbl_Guess.Name = "lbl_Guess";
            this.lbl_Guess.Size = new System.Drawing.Size(111, 13);
            this.lbl_Guess.TabIndex = 102;
            this.lbl_Guess.Text = "Enter Guess (e.g. A5):";
            // 
            // cmb_Fire
            // 
            this.cmb_Fire.ForeColor = System.Drawing.Color.Red;
            this.cmb_Fire.Location = new System.Drawing.Point(182, 75);
            this.cmb_Fire.Name = "cmb_Fire";
            this.cmb_Fire.Size = new System.Drawing.Size(36, 23);
            this.cmb_Fire.TabIndex = 1;
            this.cmb_Fire.Text = "Fire!";
            this.cmb_Fire.UseVisualStyleBackColor = true;
            this.cmb_Fire.Click += new System.EventHandler(this.cmb_Fire_Click);
            // 
            // pbxJ10
            // 
            this.pbxJ10.Location = new System.Drawing.Point(443, 482);
            this.pbxJ10.Name = "pbxJ10";
            this.pbxJ10.Size = new System.Drawing.Size(36, 33);
            this.pbxJ10.TabIndex = 100;
            this.pbxJ10.TabStop = false;
            // 
            // pbxI10
            // 
            this.pbxI10.Location = new System.Drawing.Point(401, 482);
            this.pbxI10.Name = "pbxI10";
            this.pbxI10.Size = new System.Drawing.Size(36, 33);
            this.pbxI10.TabIndex = 99;
            this.pbxI10.TabStop = false;
            // 
            // pbxH10
            // 
            this.pbxH10.Location = new System.Drawing.Point(359, 482);
            this.pbxH10.Name = "pbxH10";
            this.pbxH10.Size = new System.Drawing.Size(36, 33);
            this.pbxH10.TabIndex = 98;
            this.pbxH10.TabStop = false;
            // 
            // pbxG10
            // 
            this.pbxG10.Location = new System.Drawing.Point(317, 482);
            this.pbxG10.Name = "pbxG10";
            this.pbxG10.Size = new System.Drawing.Size(36, 33);
            this.pbxG10.TabIndex = 97;
            this.pbxG10.TabStop = false;
            // 
            // pbxF10
            // 
            this.pbxF10.Location = new System.Drawing.Point(275, 482);
            this.pbxF10.Name = "pbxF10";
            this.pbxF10.Size = new System.Drawing.Size(36, 33);
            this.pbxF10.TabIndex = 96;
            this.pbxF10.TabStop = false;
            // 
            // pbxE10
            // 
            this.pbxE10.Location = new System.Drawing.Point(233, 482);
            this.pbxE10.Name = "pbxE10";
            this.pbxE10.Size = new System.Drawing.Size(36, 33);
            this.pbxE10.TabIndex = 95;
            this.pbxE10.TabStop = false;
            // 
            // pbxD10
            // 
            this.pbxD10.Location = new System.Drawing.Point(191, 482);
            this.pbxD10.Name = "pbxD10";
            this.pbxD10.Size = new System.Drawing.Size(36, 33);
            this.pbxD10.TabIndex = 94;
            this.pbxD10.TabStop = false;
            // 
            // pbxC10
            // 
            this.pbxC10.Location = new System.Drawing.Point(150, 482);
            this.pbxC10.Name = "pbxC10";
            this.pbxC10.Size = new System.Drawing.Size(36, 33);
            this.pbxC10.TabIndex = 93;
            this.pbxC10.TabStop = false;
            // 
            // pbxB10
            // 
            this.pbxB10.Location = new System.Drawing.Point(108, 482);
            this.pbxB10.Name = "pbxB10";
            this.pbxB10.Size = new System.Drawing.Size(36, 33);
            this.pbxB10.TabIndex = 92;
            this.pbxB10.TabStop = false;
            // 
            // pbxA10
            // 
            this.pbxA10.Location = new System.Drawing.Point(67, 482);
            this.pbxA10.Name = "pbxA10";
            this.pbxA10.Size = new System.Drawing.Size(36, 33);
            this.pbxA10.TabIndex = 91;
            this.pbxA10.TabStop = false;
            // 
            // pbxJ9
            // 
            this.pbxJ9.Location = new System.Drawing.Point(443, 444);
            this.pbxJ9.Name = "pbxJ9";
            this.pbxJ9.Size = new System.Drawing.Size(36, 33);
            this.pbxJ9.TabIndex = 90;
            this.pbxJ9.TabStop = false;
            // 
            // pbxI9
            // 
            this.pbxI9.Location = new System.Drawing.Point(401, 444);
            this.pbxI9.Name = "pbxI9";
            this.pbxI9.Size = new System.Drawing.Size(36, 33);
            this.pbxI9.TabIndex = 89;
            this.pbxI9.TabStop = false;
            // 
            // pbxH9
            // 
            this.pbxH9.Location = new System.Drawing.Point(359, 444);
            this.pbxH9.Name = "pbxH9";
            this.pbxH9.Size = new System.Drawing.Size(36, 33);
            this.pbxH9.TabIndex = 88;
            this.pbxH9.TabStop = false;
            // 
            // pbxG9
            // 
            this.pbxG9.Location = new System.Drawing.Point(317, 444);
            this.pbxG9.Name = "pbxG9";
            this.pbxG9.Size = new System.Drawing.Size(36, 33);
            this.pbxG9.TabIndex = 87;
            this.pbxG9.TabStop = false;
            // 
            // pbxF9
            // 
            this.pbxF9.Location = new System.Drawing.Point(275, 444);
            this.pbxF9.Name = "pbxF9";
            this.pbxF9.Size = new System.Drawing.Size(36, 33);
            this.pbxF9.TabIndex = 86;
            this.pbxF9.TabStop = false;
            // 
            // pbxE9
            // 
            this.pbxE9.Location = new System.Drawing.Point(233, 444);
            this.pbxE9.Name = "pbxE9";
            this.pbxE9.Size = new System.Drawing.Size(36, 33);
            this.pbxE9.TabIndex = 85;
            this.pbxE9.TabStop = false;
            // 
            // pbxD9
            // 
            this.pbxD9.Location = new System.Drawing.Point(191, 444);
            this.pbxD9.Name = "pbxD9";
            this.pbxD9.Size = new System.Drawing.Size(36, 33);
            this.pbxD9.TabIndex = 84;
            this.pbxD9.TabStop = false;
            // 
            // pbxC9
            // 
            this.pbxC9.Location = new System.Drawing.Point(150, 444);
            this.pbxC9.Name = "pbxC9";
            this.pbxC9.Size = new System.Drawing.Size(36, 33);
            this.pbxC9.TabIndex = 83;
            this.pbxC9.TabStop = false;
            // 
            // pbxB9
            // 
            this.pbxB9.Location = new System.Drawing.Point(108, 444);
            this.pbxB9.Name = "pbxB9";
            this.pbxB9.Size = new System.Drawing.Size(36, 33);
            this.pbxB9.TabIndex = 82;
            this.pbxB9.TabStop = false;
            // 
            // pbxA9
            // 
            this.pbxA9.Location = new System.Drawing.Point(67, 444);
            this.pbxA9.Name = "pbxA9";
            this.pbxA9.Size = new System.Drawing.Size(36, 33);
            this.pbxA9.TabIndex = 81;
            this.pbxA9.TabStop = false;
            // 
            // pbxJ8
            // 
            this.pbxJ8.Location = new System.Drawing.Point(443, 406);
            this.pbxJ8.Name = "pbxJ8";
            this.pbxJ8.Size = new System.Drawing.Size(36, 33);
            this.pbxJ8.TabIndex = 80;
            this.pbxJ8.TabStop = false;
            // 
            // pbxI8
            // 
            this.pbxI8.Location = new System.Drawing.Point(401, 406);
            this.pbxI8.Name = "pbxI8";
            this.pbxI8.Size = new System.Drawing.Size(36, 33);
            this.pbxI8.TabIndex = 79;
            this.pbxI8.TabStop = false;
            // 
            // pbxH8
            // 
            this.pbxH8.Location = new System.Drawing.Point(359, 406);
            this.pbxH8.Name = "pbxH8";
            this.pbxH8.Size = new System.Drawing.Size(36, 33);
            this.pbxH8.TabIndex = 78;
            this.pbxH8.TabStop = false;
            // 
            // pbxG8
            // 
            this.pbxG8.Location = new System.Drawing.Point(317, 406);
            this.pbxG8.Name = "pbxG8";
            this.pbxG8.Size = new System.Drawing.Size(36, 33);
            this.pbxG8.TabIndex = 77;
            this.pbxG8.TabStop = false;
            // 
            // pbxF8
            // 
            this.pbxF8.Location = new System.Drawing.Point(275, 406);
            this.pbxF8.Name = "pbxF8";
            this.pbxF8.Size = new System.Drawing.Size(36, 33);
            this.pbxF8.TabIndex = 76;
            this.pbxF8.TabStop = false;
            // 
            // pbxE8
            // 
            this.pbxE8.Location = new System.Drawing.Point(233, 406);
            this.pbxE8.Name = "pbxE8";
            this.pbxE8.Size = new System.Drawing.Size(36, 33);
            this.pbxE8.TabIndex = 75;
            this.pbxE8.TabStop = false;
            // 
            // pbxD8
            // 
            this.pbxD8.Location = new System.Drawing.Point(191, 406);
            this.pbxD8.Name = "pbxD8";
            this.pbxD8.Size = new System.Drawing.Size(36, 33);
            this.pbxD8.TabIndex = 74;
            this.pbxD8.TabStop = false;
            // 
            // pbxC8
            // 
            this.pbxC8.Location = new System.Drawing.Point(150, 406);
            this.pbxC8.Name = "pbxC8";
            this.pbxC8.Size = new System.Drawing.Size(36, 33);
            this.pbxC8.TabIndex = 73;
            this.pbxC8.TabStop = false;
            // 
            // pbxB8
            // 
            this.pbxB8.Location = new System.Drawing.Point(108, 406);
            this.pbxB8.Name = "pbxB8";
            this.pbxB8.Size = new System.Drawing.Size(36, 33);
            this.pbxB8.TabIndex = 72;
            this.pbxB8.TabStop = false;
            // 
            // pbxA8
            // 
            this.pbxA8.Location = new System.Drawing.Point(67, 406);
            this.pbxA8.Name = "pbxA8";
            this.pbxA8.Size = new System.Drawing.Size(36, 33);
            this.pbxA8.TabIndex = 71;
            this.pbxA8.TabStop = false;
            // 
            // pbxJ7
            // 
            this.pbxJ7.Location = new System.Drawing.Point(443, 368);
            this.pbxJ7.Name = "pbxJ7";
            this.pbxJ7.Size = new System.Drawing.Size(36, 33);
            this.pbxJ7.TabIndex = 70;
            this.pbxJ7.TabStop = false;
            // 
            // pbxI7
            // 
            this.pbxI7.Location = new System.Drawing.Point(401, 368);
            this.pbxI7.Name = "pbxI7";
            this.pbxI7.Size = new System.Drawing.Size(36, 33);
            this.pbxI7.TabIndex = 69;
            this.pbxI7.TabStop = false;
            // 
            // pbxH7
            // 
            this.pbxH7.Location = new System.Drawing.Point(359, 368);
            this.pbxH7.Name = "pbxH7";
            this.pbxH7.Size = new System.Drawing.Size(36, 33);
            this.pbxH7.TabIndex = 68;
            this.pbxH7.TabStop = false;
            // 
            // pbxG7
            // 
            this.pbxG7.Location = new System.Drawing.Point(317, 368);
            this.pbxG7.Name = "pbxG7";
            this.pbxG7.Size = new System.Drawing.Size(36, 33);
            this.pbxG7.TabIndex = 67;
            this.pbxG7.TabStop = false;
            // 
            // pbxF7
            // 
            this.pbxF7.Location = new System.Drawing.Point(275, 368);
            this.pbxF7.Name = "pbxF7";
            this.pbxF7.Size = new System.Drawing.Size(36, 33);
            this.pbxF7.TabIndex = 66;
            this.pbxF7.TabStop = false;
            // 
            // pbxE7
            // 
            this.pbxE7.Location = new System.Drawing.Point(233, 368);
            this.pbxE7.Name = "pbxE7";
            this.pbxE7.Size = new System.Drawing.Size(36, 33);
            this.pbxE7.TabIndex = 65;
            this.pbxE7.TabStop = false;
            // 
            // pbxD7
            // 
            this.pbxD7.Location = new System.Drawing.Point(191, 368);
            this.pbxD7.Name = "pbxD7";
            this.pbxD7.Size = new System.Drawing.Size(36, 33);
            this.pbxD7.TabIndex = 64;
            this.pbxD7.TabStop = false;
            // 
            // pbxC7
            // 
            this.pbxC7.Location = new System.Drawing.Point(150, 368);
            this.pbxC7.Name = "pbxC7";
            this.pbxC7.Size = new System.Drawing.Size(36, 33);
            this.pbxC7.TabIndex = 63;
            this.pbxC7.TabStop = false;
            // 
            // pbxB7
            // 
            this.pbxB7.Location = new System.Drawing.Point(108, 368);
            this.pbxB7.Name = "pbxB7";
            this.pbxB7.Size = new System.Drawing.Size(36, 33);
            this.pbxB7.TabIndex = 62;
            this.pbxB7.TabStop = false;
            // 
            // pbxA7
            // 
            this.pbxA7.Location = new System.Drawing.Point(67, 368);
            this.pbxA7.Name = "pbxA7";
            this.pbxA7.Size = new System.Drawing.Size(36, 33);
            this.pbxA7.TabIndex = 61;
            this.pbxA7.TabStop = false;
            // 
            // pbxJ6
            // 
            this.pbxJ6.Location = new System.Drawing.Point(443, 331);
            this.pbxJ6.Name = "pbxJ6";
            this.pbxJ6.Size = new System.Drawing.Size(36, 33);
            this.pbxJ6.TabIndex = 60;
            this.pbxJ6.TabStop = false;
            // 
            // pbxI6
            // 
            this.pbxI6.Location = new System.Drawing.Point(401, 331);
            this.pbxI6.Name = "pbxI6";
            this.pbxI6.Size = new System.Drawing.Size(36, 33);
            this.pbxI6.TabIndex = 59;
            this.pbxI6.TabStop = false;
            // 
            // pbxH6
            // 
            this.pbxH6.Location = new System.Drawing.Point(359, 331);
            this.pbxH6.Name = "pbxH6";
            this.pbxH6.Size = new System.Drawing.Size(36, 33);
            this.pbxH6.TabIndex = 58;
            this.pbxH6.TabStop = false;
            // 
            // pbxG6
            // 
            this.pbxG6.Location = new System.Drawing.Point(317, 331);
            this.pbxG6.Name = "pbxG6";
            this.pbxG6.Size = new System.Drawing.Size(36, 33);
            this.pbxG6.TabIndex = 57;
            this.pbxG6.TabStop = false;
            // 
            // pbxF6
            // 
            this.pbxF6.Location = new System.Drawing.Point(275, 331);
            this.pbxF6.Name = "pbxF6";
            this.pbxF6.Size = new System.Drawing.Size(36, 33);
            this.pbxF6.TabIndex = 56;
            this.pbxF6.TabStop = false;
            // 
            // pbxE6
            // 
            this.pbxE6.Location = new System.Drawing.Point(233, 331);
            this.pbxE6.Name = "pbxE6";
            this.pbxE6.Size = new System.Drawing.Size(36, 33);
            this.pbxE6.TabIndex = 55;
            this.pbxE6.TabStop = false;
            // 
            // pbxD6
            // 
            this.pbxD6.Location = new System.Drawing.Point(191, 331);
            this.pbxD6.Name = "pbxD6";
            this.pbxD6.Size = new System.Drawing.Size(36, 33);
            this.pbxD6.TabIndex = 54;
            this.pbxD6.TabStop = false;
            // 
            // pbxC6
            // 
            this.pbxC6.Location = new System.Drawing.Point(150, 331);
            this.pbxC6.Name = "pbxC6";
            this.pbxC6.Size = new System.Drawing.Size(36, 33);
            this.pbxC6.TabIndex = 53;
            this.pbxC6.TabStop = false;
            // 
            // pbxB6
            // 
            this.pbxB6.Location = new System.Drawing.Point(108, 331);
            this.pbxB6.Name = "pbxB6";
            this.pbxB6.Size = new System.Drawing.Size(36, 33);
            this.pbxB6.TabIndex = 52;
            this.pbxB6.TabStop = false;
            // 
            // pbxA6
            // 
            this.pbxA6.Location = new System.Drawing.Point(67, 331);
            this.pbxA6.Name = "pbxA6";
            this.pbxA6.Size = new System.Drawing.Size(36, 33);
            this.pbxA6.TabIndex = 51;
            this.pbxA6.TabStop = false;
            // 
            // pbxJ5
            // 
            this.pbxJ5.Location = new System.Drawing.Point(443, 292);
            this.pbxJ5.Name = "pbxJ5";
            this.pbxJ5.Size = new System.Drawing.Size(36, 33);
            this.pbxJ5.TabIndex = 50;
            this.pbxJ5.TabStop = false;
            // 
            // pbxI5
            // 
            this.pbxI5.Location = new System.Drawing.Point(401, 292);
            this.pbxI5.Name = "pbxI5";
            this.pbxI5.Size = new System.Drawing.Size(36, 33);
            this.pbxI5.TabIndex = 49;
            this.pbxI5.TabStop = false;
            // 
            // pbxH5
            // 
            this.pbxH5.Location = new System.Drawing.Point(359, 292);
            this.pbxH5.Name = "pbxH5";
            this.pbxH5.Size = new System.Drawing.Size(36, 33);
            this.pbxH5.TabIndex = 48;
            this.pbxH5.TabStop = false;
            // 
            // pbxG5
            // 
            this.pbxG5.Location = new System.Drawing.Point(317, 292);
            this.pbxG5.Name = "pbxG5";
            this.pbxG5.Size = new System.Drawing.Size(36, 33);
            this.pbxG5.TabIndex = 47;
            this.pbxG5.TabStop = false;
            // 
            // pbxF5
            // 
            this.pbxF5.Location = new System.Drawing.Point(275, 292);
            this.pbxF5.Name = "pbxF5";
            this.pbxF5.Size = new System.Drawing.Size(36, 33);
            this.pbxF5.TabIndex = 46;
            this.pbxF5.TabStop = false;
            // 
            // pbxE5
            // 
            this.pbxE5.Location = new System.Drawing.Point(233, 292);
            this.pbxE5.Name = "pbxE5";
            this.pbxE5.Size = new System.Drawing.Size(36, 33);
            this.pbxE5.TabIndex = 45;
            this.pbxE5.TabStop = false;
            // 
            // pbxD5
            // 
            this.pbxD5.Location = new System.Drawing.Point(191, 292);
            this.pbxD5.Name = "pbxD5";
            this.pbxD5.Size = new System.Drawing.Size(36, 33);
            this.pbxD5.TabIndex = 44;
            this.pbxD5.TabStop = false;
            // 
            // pbxC5
            // 
            this.pbxC5.Location = new System.Drawing.Point(150, 292);
            this.pbxC5.Name = "pbxC5";
            this.pbxC5.Size = new System.Drawing.Size(36, 33);
            this.pbxC5.TabIndex = 43;
            this.pbxC5.TabStop = false;
            // 
            // pbxB5
            // 
            this.pbxB5.Location = new System.Drawing.Point(108, 292);
            this.pbxB5.Name = "pbxB5";
            this.pbxB5.Size = new System.Drawing.Size(36, 33);
            this.pbxB5.TabIndex = 42;
            this.pbxB5.TabStop = false;
            // 
            // pbxA5
            // 
            this.pbxA5.Location = new System.Drawing.Point(67, 292);
            this.pbxA5.Name = "pbxA5";
            this.pbxA5.Size = new System.Drawing.Size(36, 33);
            this.pbxA5.TabIndex = 41;
            this.pbxA5.TabStop = false;
            // 
            // pbxJ4
            // 
            this.pbxJ4.Location = new System.Drawing.Point(443, 255);
            this.pbxJ4.Name = "pbxJ4";
            this.pbxJ4.Size = new System.Drawing.Size(36, 33);
            this.pbxJ4.TabIndex = 40;
            this.pbxJ4.TabStop = false;
            // 
            // pbxI4
            // 
            this.pbxI4.Location = new System.Drawing.Point(401, 255);
            this.pbxI4.Name = "pbxI4";
            this.pbxI4.Size = new System.Drawing.Size(36, 33);
            this.pbxI4.TabIndex = 39;
            this.pbxI4.TabStop = false;
            // 
            // pbxH4
            // 
            this.pbxH4.Location = new System.Drawing.Point(359, 255);
            this.pbxH4.Name = "pbxH4";
            this.pbxH4.Size = new System.Drawing.Size(36, 33);
            this.pbxH4.TabIndex = 38;
            this.pbxH4.TabStop = false;
            // 
            // pbxG4
            // 
            this.pbxG4.Location = new System.Drawing.Point(317, 255);
            this.pbxG4.Name = "pbxG4";
            this.pbxG4.Size = new System.Drawing.Size(36, 33);
            this.pbxG4.TabIndex = 37;
            this.pbxG4.TabStop = false;
            // 
            // pbxF4
            // 
            this.pbxF4.Location = new System.Drawing.Point(275, 255);
            this.pbxF4.Name = "pbxF4";
            this.pbxF4.Size = new System.Drawing.Size(36, 33);
            this.pbxF4.TabIndex = 36;
            this.pbxF4.TabStop = false;
            // 
            // pbxE4
            // 
            this.pbxE4.Location = new System.Drawing.Point(233, 255);
            this.pbxE4.Name = "pbxE4";
            this.pbxE4.Size = new System.Drawing.Size(36, 33);
            this.pbxE4.TabIndex = 35;
            this.pbxE4.TabStop = false;
            // 
            // pbxD4
            // 
            this.pbxD4.Location = new System.Drawing.Point(191, 255);
            this.pbxD4.Name = "pbxD4";
            this.pbxD4.Size = new System.Drawing.Size(36, 33);
            this.pbxD4.TabIndex = 34;
            this.pbxD4.TabStop = false;
            // 
            // pbxC4
            // 
            this.pbxC4.Location = new System.Drawing.Point(150, 255);
            this.pbxC4.Name = "pbxC4";
            this.pbxC4.Size = new System.Drawing.Size(36, 33);
            this.pbxC4.TabIndex = 33;
            this.pbxC4.TabStop = false;
            // 
            // pbxB4
            // 
            this.pbxB4.Location = new System.Drawing.Point(108, 255);
            this.pbxB4.Name = "pbxB4";
            this.pbxB4.Size = new System.Drawing.Size(36, 33);
            this.pbxB4.TabIndex = 32;
            this.pbxB4.TabStop = false;
            // 
            // pbxA4
            // 
            this.pbxA4.Location = new System.Drawing.Point(67, 255);
            this.pbxA4.Name = "pbxA4";
            this.pbxA4.Size = new System.Drawing.Size(36, 33);
            this.pbxA4.TabIndex = 31;
            this.pbxA4.TabStop = false;
            // 
            // pbxJ3
            // 
            this.pbxJ3.Location = new System.Drawing.Point(443, 217);
            this.pbxJ3.Name = "pbxJ3";
            this.pbxJ3.Size = new System.Drawing.Size(36, 33);
            this.pbxJ3.TabIndex = 30;
            this.pbxJ3.TabStop = false;
            // 
            // pbxI3
            // 
            this.pbxI3.Location = new System.Drawing.Point(401, 217);
            this.pbxI3.Name = "pbxI3";
            this.pbxI3.Size = new System.Drawing.Size(36, 33);
            this.pbxI3.TabIndex = 29;
            this.pbxI3.TabStop = false;
            // 
            // pbxH3
            // 
            this.pbxH3.Location = new System.Drawing.Point(359, 217);
            this.pbxH3.Name = "pbxH3";
            this.pbxH3.Size = new System.Drawing.Size(36, 33);
            this.pbxH3.TabIndex = 28;
            this.pbxH3.TabStop = false;
            // 
            // pbxG3
            // 
            this.pbxG3.Location = new System.Drawing.Point(317, 217);
            this.pbxG3.Name = "pbxG3";
            this.pbxG3.Size = new System.Drawing.Size(36, 33);
            this.pbxG3.TabIndex = 27;
            this.pbxG3.TabStop = false;
            // 
            // pbxF3
            // 
            this.pbxF3.Location = new System.Drawing.Point(275, 217);
            this.pbxF3.Name = "pbxF3";
            this.pbxF3.Size = new System.Drawing.Size(36, 33);
            this.pbxF3.TabIndex = 26;
            this.pbxF3.TabStop = false;
            // 
            // pbxE3
            // 
            this.pbxE3.Location = new System.Drawing.Point(233, 217);
            this.pbxE3.Name = "pbxE3";
            this.pbxE3.Size = new System.Drawing.Size(36, 33);
            this.pbxE3.TabIndex = 25;
            this.pbxE3.TabStop = false;
            // 
            // pbxD3
            // 
            this.pbxD3.Location = new System.Drawing.Point(191, 217);
            this.pbxD3.Name = "pbxD3";
            this.pbxD3.Size = new System.Drawing.Size(36, 33);
            this.pbxD3.TabIndex = 24;
            this.pbxD3.TabStop = false;
            // 
            // pbxC3
            // 
            this.pbxC3.Location = new System.Drawing.Point(150, 217);
            this.pbxC3.Name = "pbxC3";
            this.pbxC3.Size = new System.Drawing.Size(36, 33);
            this.pbxC3.TabIndex = 23;
            this.pbxC3.TabStop = false;
            // 
            // pbxB3
            // 
            this.pbxB3.Location = new System.Drawing.Point(108, 217);
            this.pbxB3.Name = "pbxB3";
            this.pbxB3.Size = new System.Drawing.Size(36, 33);
            this.pbxB3.TabIndex = 22;
            this.pbxB3.TabStop = false;
            // 
            // pbxA3
            // 
            this.pbxA3.Location = new System.Drawing.Point(67, 217);
            this.pbxA3.Name = "pbxA3";
            this.pbxA3.Size = new System.Drawing.Size(36, 33);
            this.pbxA3.TabIndex = 21;
            this.pbxA3.TabStop = false;
            // 
            // pbxJ2
            // 
            this.pbxJ2.Location = new System.Drawing.Point(443, 180);
            this.pbxJ2.Name = "pbxJ2";
            this.pbxJ2.Size = new System.Drawing.Size(36, 33);
            this.pbxJ2.TabIndex = 20;
            this.pbxJ2.TabStop = false;
            // 
            // pbxI2
            // 
            this.pbxI2.Location = new System.Drawing.Point(401, 180);
            this.pbxI2.Name = "pbxI2";
            this.pbxI2.Size = new System.Drawing.Size(36, 33);
            this.pbxI2.TabIndex = 19;
            this.pbxI2.TabStop = false;
            // 
            // pbxH2
            // 
            this.pbxH2.Location = new System.Drawing.Point(359, 180);
            this.pbxH2.Name = "pbxH2";
            this.pbxH2.Size = new System.Drawing.Size(36, 33);
            this.pbxH2.TabIndex = 18;
            this.pbxH2.TabStop = false;
            // 
            // pbxG2
            // 
            this.pbxG2.Location = new System.Drawing.Point(317, 180);
            this.pbxG2.Name = "pbxG2";
            this.pbxG2.Size = new System.Drawing.Size(36, 33);
            this.pbxG2.TabIndex = 17;
            this.pbxG2.TabStop = false;
            // 
            // pbxF2
            // 
            this.pbxF2.Location = new System.Drawing.Point(275, 180);
            this.pbxF2.Name = "pbxF2";
            this.pbxF2.Size = new System.Drawing.Size(36, 33);
            this.pbxF2.TabIndex = 16;
            this.pbxF2.TabStop = false;
            // 
            // pbxE2
            // 
            this.pbxE2.Location = new System.Drawing.Point(233, 180);
            this.pbxE2.Name = "pbxE2";
            this.pbxE2.Size = new System.Drawing.Size(36, 33);
            this.pbxE2.TabIndex = 15;
            this.pbxE2.TabStop = false;
            // 
            // pbxD2
            // 
            this.pbxD2.Location = new System.Drawing.Point(191, 180);
            this.pbxD2.Name = "pbxD2";
            this.pbxD2.Size = new System.Drawing.Size(36, 33);
            this.pbxD2.TabIndex = 14;
            this.pbxD2.TabStop = false;
            // 
            // pbxC2
            // 
            this.pbxC2.Location = new System.Drawing.Point(150, 180);
            this.pbxC2.Name = "pbxC2";
            this.pbxC2.Size = new System.Drawing.Size(36, 33);
            this.pbxC2.TabIndex = 13;
            this.pbxC2.TabStop = false;
            // 
            // pbxB2
            // 
            this.pbxB2.Location = new System.Drawing.Point(108, 180);
            this.pbxB2.Name = "pbxB2";
            this.pbxB2.Size = new System.Drawing.Size(36, 33);
            this.pbxB2.TabIndex = 12;
            this.pbxB2.TabStop = false;
            // 
            // pbxA2
            // 
            this.pbxA2.Location = new System.Drawing.Point(67, 180);
            this.pbxA2.Name = "pbxA2";
            this.pbxA2.Size = new System.Drawing.Size(36, 33);
            this.pbxA2.TabIndex = 11;
            this.pbxA2.TabStop = false;
            // 
            // pbxJ1
            // 
            this.pbxJ1.Location = new System.Drawing.Point(443, 142);
            this.pbxJ1.Name = "pbxJ1";
            this.pbxJ1.Size = new System.Drawing.Size(36, 33);
            this.pbxJ1.TabIndex = 10;
            this.pbxJ1.TabStop = false;
            // 
            // pbxI1
            // 
            this.pbxI1.Location = new System.Drawing.Point(401, 142);
            this.pbxI1.Name = "pbxI1";
            this.pbxI1.Size = new System.Drawing.Size(36, 33);
            this.pbxI1.TabIndex = 9;
            this.pbxI1.TabStop = false;
            // 
            // pbxH1
            // 
            this.pbxH1.Location = new System.Drawing.Point(359, 142);
            this.pbxH1.Name = "pbxH1";
            this.pbxH1.Size = new System.Drawing.Size(36, 33);
            this.pbxH1.TabIndex = 8;
            this.pbxH1.TabStop = false;
            // 
            // pbxG1
            // 
            this.pbxG1.Location = new System.Drawing.Point(317, 142);
            this.pbxG1.Name = "pbxG1";
            this.pbxG1.Size = new System.Drawing.Size(36, 33);
            this.pbxG1.TabIndex = 7;
            this.pbxG1.TabStop = false;
            // 
            // pbxF1
            // 
            this.pbxF1.Location = new System.Drawing.Point(275, 142);
            this.pbxF1.Name = "pbxF1";
            this.pbxF1.Size = new System.Drawing.Size(36, 33);
            this.pbxF1.TabIndex = 6;
            this.pbxF1.TabStop = false;
            // 
            // pbxE1
            // 
            this.pbxE1.Location = new System.Drawing.Point(233, 142);
            this.pbxE1.Name = "pbxE1";
            this.pbxE1.Size = new System.Drawing.Size(36, 33);
            this.pbxE1.TabIndex = 5;
            this.pbxE1.TabStop = false;
            // 
            // pbxD1
            // 
            this.pbxD1.Location = new System.Drawing.Point(191, 142);
            this.pbxD1.Name = "pbxD1";
            this.pbxD1.Size = new System.Drawing.Size(36, 33);
            this.pbxD1.TabIndex = 4;
            this.pbxD1.TabStop = false;
            // 
            // pbxC1
            // 
            this.pbxC1.Location = new System.Drawing.Point(150, 142);
            this.pbxC1.Name = "pbxC1";
            this.pbxC1.Size = new System.Drawing.Size(36, 33);
            this.pbxC1.TabIndex = 3;
            this.pbxC1.TabStop = false;
            // 
            // pbxB1
            // 
            this.pbxB1.Location = new System.Drawing.Point(108, 142);
            this.pbxB1.Name = "pbxB1";
            this.pbxB1.Size = new System.Drawing.Size(36, 33);
            this.pbxB1.TabIndex = 2;
            this.pbxB1.TabStop = false;
            // 
            // pbxA1
            // 
            this.pbxA1.Location = new System.Drawing.Point(67, 142);
            this.pbxA1.Name = "pbxA1";
            this.pbxA1.Size = new System.Drawing.Size(36, 33);
            this.pbxA1.TabIndex = 0;
            this.pbxA1.TabStop = false;
            // 
            // pbx_Grid
            // 
            this.pbx_Grid.Image = global::Battleships_LR.Properties.Resources.Grid;
            this.pbx_Grid.InitialImage = null;
            this.pbx_Grid.Location = new System.Drawing.Point(24, 103);
            this.pbx_Grid.Name = "pbx_Grid";
            this.pbx_Grid.Size = new System.Drawing.Size(467, 424);
            this.pbx_Grid.TabIndex = 1;
            this.pbx_Grid.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Swis721 BlkEx BT", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(113, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(291, 35);
            this.label1.TabIndex = 104;
            this.label1.Text = "BATTLESHIPS";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(513, 544);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cmb_Fire);
            this.Controls.Add(this.lbl_Guess);
            this.Controls.Add(this.txt_Guess);
            this.Controls.Add(this.pbxJ10);
            this.Controls.Add(this.pbxI10);
            this.Controls.Add(this.pbxH10);
            this.Controls.Add(this.pbxG10);
            this.Controls.Add(this.pbxF10);
            this.Controls.Add(this.pbxE10);
            this.Controls.Add(this.pbxD10);
            this.Controls.Add(this.pbxC10);
            this.Controls.Add(this.pbxB10);
            this.Controls.Add(this.pbxA10);
            this.Controls.Add(this.pbxJ9);
            this.Controls.Add(this.pbxI9);
            this.Controls.Add(this.pbxH9);
            this.Controls.Add(this.pbxG9);
            this.Controls.Add(this.pbxF9);
            this.Controls.Add(this.pbxE9);
            this.Controls.Add(this.pbxD9);
            this.Controls.Add(this.pbxC9);
            this.Controls.Add(this.pbxB9);
            this.Controls.Add(this.pbxA9);
            this.Controls.Add(this.pbxJ8);
            this.Controls.Add(this.pbxI8);
            this.Controls.Add(this.pbxH8);
            this.Controls.Add(this.pbxG8);
            this.Controls.Add(this.pbxF8);
            this.Controls.Add(this.pbxE8);
            this.Controls.Add(this.pbxD8);
            this.Controls.Add(this.pbxC8);
            this.Controls.Add(this.pbxB8);
            this.Controls.Add(this.pbxA8);
            this.Controls.Add(this.pbxJ7);
            this.Controls.Add(this.pbxI7);
            this.Controls.Add(this.pbxH7);
            this.Controls.Add(this.pbxG7);
            this.Controls.Add(this.pbxF7);
            this.Controls.Add(this.pbxE7);
            this.Controls.Add(this.pbxD7);
            this.Controls.Add(this.pbxC7);
            this.Controls.Add(this.pbxB7);
            this.Controls.Add(this.pbxA7);
            this.Controls.Add(this.pbxJ6);
            this.Controls.Add(this.pbxI6);
            this.Controls.Add(this.pbxH6);
            this.Controls.Add(this.pbxG6);
            this.Controls.Add(this.pbxF6);
            this.Controls.Add(this.pbxE6);
            this.Controls.Add(this.pbxD6);
            this.Controls.Add(this.pbxC6);
            this.Controls.Add(this.pbxB6);
            this.Controls.Add(this.pbxA6);
            this.Controls.Add(this.pbxJ5);
            this.Controls.Add(this.pbxI5);
            this.Controls.Add(this.pbxH5);
            this.Controls.Add(this.pbxG5);
            this.Controls.Add(this.pbxF5);
            this.Controls.Add(this.pbxE5);
            this.Controls.Add(this.pbxD5);
            this.Controls.Add(this.pbxC5);
            this.Controls.Add(this.pbxB5);
            this.Controls.Add(this.pbxA5);
            this.Controls.Add(this.pbxJ4);
            this.Controls.Add(this.pbxI4);
            this.Controls.Add(this.pbxH4);
            this.Controls.Add(this.pbxG4);
            this.Controls.Add(this.pbxF4);
            this.Controls.Add(this.pbxE4);
            this.Controls.Add(this.pbxD4);
            this.Controls.Add(this.pbxC4);
            this.Controls.Add(this.pbxB4);
            this.Controls.Add(this.pbxA4);
            this.Controls.Add(this.pbxJ3);
            this.Controls.Add(this.pbxI3);
            this.Controls.Add(this.pbxH3);
            this.Controls.Add(this.pbxG3);
            this.Controls.Add(this.pbxF3);
            this.Controls.Add(this.pbxE3);
            this.Controls.Add(this.pbxD3);
            this.Controls.Add(this.pbxC3);
            this.Controls.Add(this.pbxB3);
            this.Controls.Add(this.pbxA3);
            this.Controls.Add(this.pbxJ2);
            this.Controls.Add(this.pbxI2);
            this.Controls.Add(this.pbxH2);
            this.Controls.Add(this.pbxG2);
            this.Controls.Add(this.pbxF2);
            this.Controls.Add(this.pbxE2);
            this.Controls.Add(this.pbxD2);
            this.Controls.Add(this.pbxC2);
            this.Controls.Add(this.pbxB2);
            this.Controls.Add(this.pbxA2);
            this.Controls.Add(this.pbxJ1);
            this.Controls.Add(this.pbxI1);
            this.Controls.Add(this.pbxH1);
            this.Controls.Add(this.pbxG1);
            this.Controls.Add(this.pbxF1);
            this.Controls.Add(this.pbxE1);
            this.Controls.Add(this.pbxD1);
            this.Controls.Add(this.pbxC1);
            this.Controls.Add(this.pbxB1);
            this.Controls.Add(this.pbxA1);
            this.Controls.Add(this.pbx_Grid);
            this.Name = "Form1";
            this.Text = "Lee Raybone Battleships";
            ((System.ComponentModel.ISupportInitialize)(this.pbxJ10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxI10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxH10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxG10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxF10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxE10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxD10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxC10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxB10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxA10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxJ9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxI9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxH9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxG9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxF9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxE9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxD9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxC9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxB9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxA9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxJ8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxI8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxH8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxG8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxF8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxE8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxD8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxC8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxB8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxA8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxJ7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxI7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxH7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxG7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxF7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxE7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxD7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxC7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxB7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxA7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxJ6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxI6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxH6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxG6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxF6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxE6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxD6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxC6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxB6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxA6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxJ5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxI5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxH5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxG5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxF5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxE5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxD5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxC5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxB5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxA5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxJ4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxI4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxH4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxG4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxF4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxE4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxD4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxC4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxB4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxA4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxJ3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxI3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxH3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxG3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxF3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxE3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxD3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxC3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxB3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxA3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxJ2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxI2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxH2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxG2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxF2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxE2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxD2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxC2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxB2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxA2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxJ1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxI1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxH1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxG1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxF1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxE1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxD1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxC1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxB1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxA1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_Grid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pbxA1;
        private System.Windows.Forms.PictureBox pbx_Grid;
        private System.Windows.Forms.PictureBox pbxB1;
        private System.Windows.Forms.PictureBox pbxC1;
        private System.Windows.Forms.PictureBox pbxD1;
        private System.Windows.Forms.PictureBox pbxE1;
        private System.Windows.Forms.PictureBox pbxF1;
        private System.Windows.Forms.PictureBox pbxG1;
        private System.Windows.Forms.PictureBox pbxH1;
        private System.Windows.Forms.PictureBox pbxI1;
        private System.Windows.Forms.PictureBox pbxJ1;
        private System.Windows.Forms.PictureBox pbxJ2;
        private System.Windows.Forms.PictureBox pbxI2;
        private System.Windows.Forms.PictureBox pbxH2;
        private System.Windows.Forms.PictureBox pbxG2;
        private System.Windows.Forms.PictureBox pbxF2;
        private System.Windows.Forms.PictureBox pbxE2;
        private System.Windows.Forms.PictureBox pbxD2;
        private System.Windows.Forms.PictureBox pbxC2;
        private System.Windows.Forms.PictureBox pbxB2;
        private System.Windows.Forms.PictureBox pbxA2;
        private System.Windows.Forms.PictureBox pbxJ3;
        private System.Windows.Forms.PictureBox pbxI3;
        private System.Windows.Forms.PictureBox pbxH3;
        private System.Windows.Forms.PictureBox pbxG3;
        private System.Windows.Forms.PictureBox pbxF3;
        private System.Windows.Forms.PictureBox pbxE3;
        private System.Windows.Forms.PictureBox pbxD3;
        private System.Windows.Forms.PictureBox pbxC3;
        private System.Windows.Forms.PictureBox pbxB3;
        private System.Windows.Forms.PictureBox pbxA3;
        private System.Windows.Forms.PictureBox pbxJ4;
        private System.Windows.Forms.PictureBox pbxI4;
        private System.Windows.Forms.PictureBox pbxH4;
        private System.Windows.Forms.PictureBox pbxG4;
        private System.Windows.Forms.PictureBox pbxF4;
        private System.Windows.Forms.PictureBox pbxE4;
        private System.Windows.Forms.PictureBox pbxD4;
        private System.Windows.Forms.PictureBox pbxC4;
        private System.Windows.Forms.PictureBox pbxB4;
        private System.Windows.Forms.PictureBox pbxA4;
        private System.Windows.Forms.PictureBox pbxJ5;
        private System.Windows.Forms.PictureBox pbxI5;
        private System.Windows.Forms.PictureBox pbxH5;
        private System.Windows.Forms.PictureBox pbxG5;
        private System.Windows.Forms.PictureBox pbxF5;
        private System.Windows.Forms.PictureBox pbxE5;
        private System.Windows.Forms.PictureBox pbxD5;
        private System.Windows.Forms.PictureBox pbxC5;
        private System.Windows.Forms.PictureBox pbxB5;
        private System.Windows.Forms.PictureBox pbxA5;
        private System.Windows.Forms.PictureBox pbxJ6;
        private System.Windows.Forms.PictureBox pbxI6;
        private System.Windows.Forms.PictureBox pbxH6;
        private System.Windows.Forms.PictureBox pbxG6;
        private System.Windows.Forms.PictureBox pbxF6;
        private System.Windows.Forms.PictureBox pbxE6;
        private System.Windows.Forms.PictureBox pbxD6;
        private System.Windows.Forms.PictureBox pbxC6;
        private System.Windows.Forms.PictureBox pbxB6;
        private System.Windows.Forms.PictureBox pbxA6;
        private System.Windows.Forms.PictureBox pbxJ7;
        private System.Windows.Forms.PictureBox pbxI7;
        private System.Windows.Forms.PictureBox pbxH7;
        private System.Windows.Forms.PictureBox pbxG7;
        private System.Windows.Forms.PictureBox pbxF7;
        private System.Windows.Forms.PictureBox pbxE7;
        private System.Windows.Forms.PictureBox pbxD7;
        private System.Windows.Forms.PictureBox pbxC7;
        private System.Windows.Forms.PictureBox pbxB7;
        private System.Windows.Forms.PictureBox pbxA7;
        private System.Windows.Forms.PictureBox pbxJ8;
        private System.Windows.Forms.PictureBox pbxI8;
        private System.Windows.Forms.PictureBox pbxH8;
        private System.Windows.Forms.PictureBox pbxG8;
        private System.Windows.Forms.PictureBox pbxF8;
        private System.Windows.Forms.PictureBox pbxE8;
        private System.Windows.Forms.PictureBox pbxD8;
        private System.Windows.Forms.PictureBox pbxC8;
        private System.Windows.Forms.PictureBox pbxB8;
        private System.Windows.Forms.PictureBox pbxA8;
        private System.Windows.Forms.PictureBox pbxJ9;
        private System.Windows.Forms.PictureBox pbxI9;
        private System.Windows.Forms.PictureBox pbxH9;
        private System.Windows.Forms.PictureBox pbxG9;
        private System.Windows.Forms.PictureBox pbxF9;
        private System.Windows.Forms.PictureBox pbxE9;
        private System.Windows.Forms.PictureBox pbxD9;
        private System.Windows.Forms.PictureBox pbxC9;
        private System.Windows.Forms.PictureBox pbxB9;
        private System.Windows.Forms.PictureBox pbxA9;
        private System.Windows.Forms.PictureBox pbxJ10;
        private System.Windows.Forms.PictureBox pbxI10;
        private System.Windows.Forms.PictureBox pbxH10;
        private System.Windows.Forms.PictureBox pbxG10;
        private System.Windows.Forms.PictureBox pbxF10;
        private System.Windows.Forms.PictureBox pbxE10;
        private System.Windows.Forms.PictureBox pbxD10;
        private System.Windows.Forms.PictureBox pbxC10;
        private System.Windows.Forms.PictureBox pbxB10;
        private System.Windows.Forms.PictureBox pbxA10;
        private System.Windows.Forms.TextBox txt_Guess;
        private System.Windows.Forms.Label lbl_Guess;
        private System.Windows.Forms.Button cmb_Fire;
        private System.Windows.Forms.Label label1;
    }
}

