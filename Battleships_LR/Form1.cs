﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Battleships_LR
{
    public partial class Form1 : Form
    {
        Random rand = new Random();
        string[] BattleShip = { "BS", "BS", "BS", "BS", "BS" };
        string[] Destroyer1 = { "DS1", "DS1", "DS1", "DS1" };
        string[] Destroyer2 = { "DS2", "DS2", "DS2", "DS2" };
        Dictionary<int, string[]> ships = new Dictionary<int, string[]>();
        string[,] gridMatrix = new string[10, 10];
        int hitCount = 0;
        int score = 0;
        int BattleShipHitCount = 0;
        int Destroyer1HitCount = 0;
        int Destroyer2HitCount = 0;

        public Form1()
        {
            InitializeComponent();
            cls_Functions.AttachKeyDownToControls(this);
            string[] gridArray = new string[100];
            ships.Add(1, BattleShip);
            ships.Add(2, Destroyer1);
            ships.Add(3, Destroyer2);
            int NoShips = ships.Count;
            int rndCol = 0;
            int rndRow = 0;
            
            for (int i =1; i <= NoShips; i++)
            {
                foreach (var item in ships)
                {
                    if (item.Key == i)
                    {
                        hitCount += item.Value.Length;
                       bool placing = cls_Functions.NextBool(rand, 50);
                        if (placing)
                        {
                            rndCol = rand.Next(0, 10);
                            rndRow = rand.Next(0, 9 - item.Value.Length);
                        }
                        else
                        {
                            rndCol = rand.Next(0, 9 - item.Value.Length);
                            rndRow = rand.Next(0, 10);
                        }

                        gridMatrix[rndRow, rndCol] = item.Value[0];
                        int increment = 1;
                        for (int j =1; j < item.Value.Length; j++)
                        {
                            

                            if (placing)
                            { 
                                gridMatrix[rndRow + increment, rndCol] = item.Value[j];
                                increment += 1;
                            }
                            else
                            {
                                gridMatrix[rndRow, rndCol + increment] = item.Value[j];
                                increment += 1;
                            }
                        }
                        
                    }
                }
            }
        }

    private void cmb_Fire_Click(object sender, EventArgs e)
    {
            if (this.txt_Guess.Text!="")
            {

            string guess = this.txt_Guess.Text;
            int guessCol = cls_Functions.GetColumnNumber(cls_Functions.Left(guess, 1))-1;
            //string guessColLet = cls_Functions.Left(guess, 1);
            int guessRow = Convert.ToInt16(cls_Functions.Right(guess, guess.Length - 1))-1;

            if (gridMatrix[guessRow, guessCol] == null)
            {
                MessageBox.Show("Miss");
                string guessPbx = "pbx" + guess;
                var pictureBox = (PictureBox)this.Controls[guessPbx];
                pictureBox.Image = Properties.Resources.Miss;
                pictureBox.SizeMode = PictureBoxSizeMode.Zoom;
                pictureBox.Refresh();
                this.txt_Guess.Clear();
            }
            else
            {
                
                MessageBox.Show("Hit!");
                string guessPbx = "pbx" + guess;
                var pictureBox = (PictureBox)this.Controls[guessPbx];
                pictureBox.Image = Properties.Resources.Hit;
                pictureBox.SizeMode = PictureBoxSizeMode.Zoom;
                pictureBox.Refresh();
                this.txt_Guess.Clear();
                score += 1;
                if (gridMatrix[guessRow, guessCol] == "BS")
                {
                    BattleShipHitCount += 1;
                    if (BattleShipHitCount == BattleShip.Length)
                    {
                        MessageBox.Show("You SUNK my Battleship!");
                    }
                }
                else if (gridMatrix[guessRow, guessCol] == "DS1")
                {
                    Destroyer1HitCount += 1;
                    if (Destroyer1HitCount == Destroyer1.Length)
                    {
                        MessageBox.Show("You SUNK my Destroyer!");
                    }
                }
                else if (gridMatrix[guessRow, guessCol] == "DS2")
                {
                    Destroyer2HitCount += 1;
                    if (Destroyer2HitCount == Destroyer2.Length)
                    {
                        MessageBox.Show("You SUNK my Destroyer!");
                    }
                }


                if (score== hitCount)
                {
                    MessageBox.Show("You Won Game Over!");
                    DialogResult playAgain = MessageBox.Show("Do you want to play again?", "Game Over", MessageBoxButtons.YesNo);
                    if (playAgain == DialogResult.Yes)
                    {
                        Application.Restart();
                        this.Close();
                    }
                }
            }
            }
            else
            {
                MessageBox.Show("Enter a Guess!");
                this.txt_Guess.Focus();
            }
        }


    }
}
